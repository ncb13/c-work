/* ===========================================================================
PROGRAM: Playlist cpp File

AUTHOR: Nikhil Bosshardt
FSU MAIL NAME: ncb13
RECITATION SECTION NUMBER: 6
RECITATION INSTRUCTOR NAME: David Lafontant
COURSE: COP 3330 Fall 2014
PROJECT NUMBER: 4
DUE DATE: Monday 10/27/14

=========================================================================== */

#include <iostream>
#include <iomanip>
#include "song.h"
#include "playlist.h"


using namespace std;

Playlist::Playlist()
{
	allo = 0;
	list = new Song[allo + 5];
	arraySize = 5;
}

void Playlist::Add(const char* a, const char* b, char c, int d)
{
	Song newSong;

	Style in = Convert(c);

	newSong.Set(a, b, in, d);

	list[allo] = newSong;
	allo++;

	//Resizing algorithim!
	if (allo <= (arraySize - 1))
	{
		SongPtr newList;
		newList = new Song[allo + 5];

		for (int k = 0; k < allo; k++)
		{
			newList[k] = list[k];
		}

		delete[] list;
		list = newList;

		cout << "The array has bee resized from " << arraySize;
		arraySize = (allo + 5);
		cout << " to " << arraySize << "." << endl;
	}

	cout << "Your song has been added! " << endl << endl;

}

void Playlist::Find(const char* f)
{
	bool found = false;
	cout << endl;
	cout << "Title		                   Artist	       Style   Size (MB)" << endl;

	for (int k = 0; k < (allo + 1); k++) //Copying the title array
	{
		if (0 == strcmp(list[k].GetTitle(), f))
		{
			list[k].Display();
			found = true;
		}
	}

	for (int k = 0; k < (allo + 1); k++) //Copying the title array
	{
		if (0 == strcmp(list[k].GetArtist(), f))
		{
			list[k].Display();
			found = true;
		}
	}
	if (found == false)
	{
		cout << "There are no results found!" << endl << endl;
	}
}

int Playlist::Size()
{
	int totalSize = 0;
	for (int k = 0; k < (allo); k++)
	{
		totalSize = totalSize + list[k].GetSize();
	}
	return totalSize;
}

void Playlist::Show()
{
	if (allo > 0)
	{
		cout << "Title		                   Artist	       Style   Size (MB)" << endl;

		for (int k = 0; k < (allo); k++) //Copying the title array
		{
			list[k].Display();
		}
		int totalSize = 0;
		for (int k = 0; k < (allo); k++)
		{
			totalSize = totalSize + list[k].GetSize();
		}
		
		cout << "This playlist contains " << (allo) << " song(s)." << endl;
		cout << "This playlist is " << (totalSize / 1000) << " mB in size." << endl << endl;
	}
	else
		cout << "There are no songs in this playlist!" << endl << endl;

}

void Playlist::ShowCat(char c)
{
	Style s = Convert(c);
	bool status = false;
	int totalSize = 0;
	int songCount = 0;
	cout << "Title		                   Artist	       Style   Size (MB)" << endl;

	for (int k = 0; k < (allo + 1); k++)
	{
		if (list[k].GetCategory() == s)
		{
			list[k].Display();
			status = true;
			totalSize += list[k].GetSize();
			songCount++;
			
		}
	}
	cout << "This playlist contains " << (songCount) << " song(s)." << endl;
	cout << "This playlist is " << (totalSize / 1000) << " mB in size." << endl;
	if (songCount == 0)
	{
		cout << endl << "Sorry! There are no songs in that catagory!" << endl;
	}
	cout << endl;
}

void Playlist::Delete(const char* d)
{
	int songNum = -1;

	for (int k = 0; k < (allo + 1); k++) //searching
	{
		if (0 == strcmp(list[k].GetTitle(), d))
		{
			songNum = k;
		}
	}
	if (songNum >= 0)
	{
		int k = 0;
		while (k < (songNum + 1))
		{
			list[k] = list[k + 1];
			k++;
		}

		SongPtr newList;
		newList = new Song[arraySize - 1];

		for (int k = 0; k < allo; k++)
		{
			newList[k] = list[k];
		}

		delete[] list;
		list = newList;

		cout << "The array has bee resized from " << arraySize;
		arraySize = (arraySize - 1);
		cout << " to " << arraySize << "." << endl;

		allo--;
	}
	else if (songNum < 0)
	{
		cout << endl << "Sorry! There are no songs by that name!";
	}
	cout << endl;


}

Style Playlist::Convert(char c)
{
	Style in;

	switch (c)
	{
	case 'p':
	case 'P':
		in = POP;
		break;
	case 'r':
	case 'R':
		in = ROCK;
		break;
	case 'a':
	case 'A':
		in = ALTERNATIVE;
		break;
	case 'C':
	case 'c':
		in = COUNTRY;
		break;
	case 'H':
	case 'h':
		in = HIPHOP;
		break;
	case 'y':
	case 'Y':
		in = PARODY;
		break;
	default:
		cout << "Invalid input, defaulted to POP. ";
		in = POP;
		break;
	}
	return in;

}

Playlist::~Playlist()
{
	delete[] list;
}