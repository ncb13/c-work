/* ===========================================================================
PROGRAM: Song Class cpp File

AUTHOR: Nikhil Bosshardt
FSU MAIL NAME: ncb13
RECITATION SECTION NUMBER: 6
RECITATION INSTRUCTOR NAME: David Lafontant
COURSE: COP 3330 Fall 2014
PROJECT NUMBER: 4
DUE DATE: Monday 10/27/14

=========================================================================== */

#include <iostream>
#include <iomanip>
#include "song.h"
#include "playlist.h"

using namespace std;

Song::Song() //Constructor
{
	//nothing!
}

void Song::Set(const char* t, const char* a, Style st, int sz)
{
	if (sz < 0) //Setting size and error check
	{
		size = 0;
	}
	else if (sz > 0)
	{
		size = sz;
	}

	category = st; //Setting the category

	for (int k = 0; k < 37; k++) //Copying the title array
	{
		title[k] = t[k];
	}

	for (int k = 0; k < 22; k++) //Copying the artist array
	{
		artist[k] = a[k];
	}
}

const char* Song::GetTitle() const
{
	return title;
}

const char* Song::GetArtist() const
{
	return artist;
}

int Song::GetSize() const
{
	return size;
}

Style Song::GetCategory() const
{
	return category;
}

void Song::Display() const
{
	int k = 0;
	int s = 0;

	while (title[k] != '\0')
	{
		cout << title[k];
		k++;
	}
	while (s < (35 - k))
	{
		cout << " ";
		s++;
	}

	s = 0;
	k = 0;
	while (artist[k] != '\0')
	{
		cout << artist[k];
		k++;
	}

	while (s < (20 - k))
	{
		cout << " ";
		s++;
	}

	switch (category)
	{
	case POP:
		cout << "Pop ";
		break;
	case ROCK:
		cout << "Rock";
		break;
	case ALTERNATIVE:
		cout << "Alt ";
		break;
	case COUNTRY:
		cout << "Ctry";
		break;
	case HIPHOP:
		cout << "HH  ";
		break;
	case PARODY:
		cout << "Par ";
		break;
	}
	cout << "      ";
	double finalSize = size / 1000.0; //Printing the size
	cout << setprecision(1) << fixed << finalSize << endl;
}