/* ===========================================================================
PROGRAM: menu cpp File

AUTHOR: Nikhil Bosshardt
FSU MAIL NAME: ncb13
RECITATION SECTION NUMBER: 6
RECITATION INSTRUCTOR NAME: David Lafontant
COURSE: COP 3330 Fall 2014
PROJECT NUMBER: 4
DUE DATE: Monday 10/27/14

=========================================================================== */

#include <iostream>
#include <iomanip>
#include "song.h"
#include "playlist.h"


using namespace std;

int main()
{
	Playlist myPlayList;
	char input;

	do{
		cout << "A:  Add a song to the playlist" << endl;
		cout << "F : Find a song on the playlist" << endl;
		cout << "D : Delete a song from the playlist" << endl;
		cout << "S : Show the entire playlist" << endl;
		cout << "C : Category summary" << endl;
		cout << "Z : Show playlist size" << endl;
		cout << "M : Show this Menu" << endl;
		cout << "X : eXit the program" << endl;
		cin >> input;

		if (input == 'A' || input == 'a')
		{
			char inTitle[36];
			char inArtist[21];
			char c;
			int size = 0;
			
			cin.getline(inTitle, 1); //Used to clear
			
			cout << endl << "Enter song title: ";
			cin.getline(inTitle, 35);
			
			cout << "Enter the song's artist: ";
			cin.getline(inArtist, 20);
			
			cout << "Enter the category of the song (1 letter): ";
			cin >> c;
			cin.clear();
			
			while (c != 'p' && c != 'P' && c != 'r' && c != 'R' && c != 'a' &&
				c != 'A' && c != 'C' && c != 'c' && c != 'h' && c != 'H' &&
				c != 'Y' && c != 'y')
			{
				cout << "Invalid! Please enter once more: ";
				cin >> c;
			}
			
			cout << "How large is the file (in kB): ";
			cin >> size;

			if (cin.fail() == true)
			{
				size = 1000;
				cout << "Incorrect input! Exiting program!" << endl;
				
			}

			myPlayList.Add(inTitle, inArtist, c, size);

		}
		
		
		else if (input == 'F' || input == 'f')
		{
			char inSource[36];
			cin.getline(inSource, 1); //Used to clear
			
			cout << "Enter the song or artist you want to find: ";
			cin.getline(inSource, 35);
			myPlayList.Find(inSource);
			cout << endl << endl << endl;
		}


		else if (input == 'D' || input == 'd')
		{
			char inSource[36];
			cin.getline(inSource, 1); //Used to clear

			cout << "Enter the name of the song to be deleted: ";
			cin.getline(inSource, 35);
			myPlayList.Delete(inSource);
			cout << endl << endl;

		}

		else if (input == 's' || input == 'S')
		{
			myPlayList.Show();
		}

		else if (input == 'C' || input == 'c')
		{
			char c;

			cout << "Enter the Category: ";
			cin >> c;
			while (c != 'p' && c != 'P' && c != 'r' && c != 'R' && c != 'a' &&
				c != 'A' && c != 'C' && c != 'c' && c != 'h' && c != 'H' &&
				c != 'Y' && c != 'y')
			{
				cout << "Invalid! Please enter once more";
				cin >> c;
			}

			myPlayList.ShowCat(c);
			
		}

		else if (input == 'Z' || input == 'z')
		{
			cout << endl << "The current size of your playlist is " <<
				myPlayList.Size() << " kB" << endl << endl;

		}

		else if (input == 'M' || input == 'm')
		{
			//Yes this will show the menu once more!
		}


		else if (input == 'X' || input == 'x')
		{
			break; //A precaution...
		}

	} while (input != 'X' || input != 'x');


	cout << "Thanks for using THE SUPER PLAYLIST MAKER 9000!" << endl;


	return 0;
}