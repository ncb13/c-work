/* ========================================================================== */
/* PROGRAM High-Low Game

    AUTHOR: Nikhil Bosshardt
    FSU MAIL NAME: ncb13
    PROJECT NUMBER: 4 
    PLATFORM: Windows OS / C++ 11 / Microsoft Visual C++ Express 2012 IDE

SUMMARY: 

This program simulates a simple high low game. The program generates a psuedo-random
number and the user attempts to guess it. The user is presented with a thousand dollars
and is allowed to bet a specific amount on whether or not his guess will be right.
After each guess the program tells the user if he is too high or too low. The user
has six tries. The user is allowed to continue playing until he is out of money.

INPUT: 

All input is interactive. The user will be prompted to both enter a bet and guess for
each random number. The user will also be asked if he wants to play agian.

OUTPUT: 

The program will prompt the user for his bet. It will then ask the user to enter thier
guess at each number. The program will also inform the user if his last guess was too high
or too low. The program will also keep the user updated on his or her current net worth and
give the percent of games won.

ASSUMPTIONS: 

That the user will try is best at guessing and not enter totally unreasonable amounts.

*/
/* ========================================================================== */

#include <iostream>
#include <iomanip>
#include <time.h> //Used in random number generation
using namespace std;

/* ########################################################### */
/* ######################  Prototypes  ####################### */
/* ########################################################### */

/*
   PrintHeading simply prints the introductory output.
   Parameters: initial amount of money received
*/
void PrintHeading(int);
 
/*
    GetBet prompts for and reads in a bet.  The function performs all
    error checking necessary to insure that a valid bet is read in
    and does not return until a valid bet is entered.
    Parameters:
        money: the amount of money the player currently has
        bet: the bet chosen by the user
*/

void GetBet(int money, int& bet);
/*
    GetGuess reads in a guess.  The user is not prompted for the guess in
    this function. The user only gets one chance to input a guess value.
    Return Value: the value of the guess if the input is valid
                  0 if the input guess was not valid
*/
int GetGuess(void);
 
/*
    CalcNewMoney determines the amount of money the player has won or
    lost during the last game.
    Parameters:
        money:   the amount of money the player had going into the game
        bet:     the amount the player bet on the current game
        guesses: the number of guesses it took the player to win.
                 -1 if the player did not guess correctly
    Return Value: the new amount of money the player has
*/
int CalcNewMoney(int money, int bet, int guessCounter);
 
/*
    PlayAgain prompts the user to play the game again and reads in a response,
    using a single character to represent a yes or no reply.
    Error checking is performed on that response.
    Return Value: true if the user wants to play again
                  false if the user does not want to play again.
*/
bool PlayAgain(void);
 
/*
    PlayGame plays a single game, performing all the necessary calculations,
    input, and output.  
    Parameters:
        money: the amount of money the player has at the start of the game.
    Return Value: how much the player has after the game.
*/
int PlayGame(int);

/* ########################################################### */
/* ###################### Main Function ###################### */
/* ########################################################### */

int main ()
{
	int money = 1000; //The users money. The starting value is 1000
	double playCount = 0; //The number of times the game is played
	double winCount = 0; //Number of wins
	double winPercent; //The percent of games the user has won
	
	PrintHeading(money); //Prints the initial heading
	
	do
	{
	playCount++; //Adds to the number of times played
	money = PlayGame(money); //Function that plays the game. Retures the users current money.

	if (money < 0) //If the money value is negative then the user has won. (See PlayGame)
		{
			money =-money; //Sets the money as positive so the rest of the program will run normally
			winCount++; //Increases the win counter.
		}

	cout << "You currently have " << money << " dollar(s)."<< endl; //Echos the amount of money the user has.
	winPercent = 100*(winCount/playCount); //Calculates the percent of wins for the user.
	cout << "You have won " << winCount << " game(s) out of " << playCount << " game(s)."
		 << " Thats " << winPercent<< "%." << endl; // Echos the users stastics
	
	cout << "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" << endl;
	
	if (money < 0 || money == 0) //Triggers if the user is out of money.
		{
			cout << "Sorry bud! You are out of money!" << endl;
			cout << "Thanks for playing." << endl;
			cout << "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" << endl;
			break; //Ends the program if the uses has no money.
		}
	}

	while (PlayAgain() == true); // Keeps going as long as the user wants to.
	
	return 0; //Generic Return
}

/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

void PrintHeading(int money) // Function to print initial heading
{
	//Heading
	cout << "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" << endl;
	cout << "=-=-=-=-=-= Welcome to the high low game=-=-=-=-=-=" << endl; 
	cout << "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" << endl;
	//Instructions
	cout << endl << "     First enter a bet. Then try to guess a number" << endl;
	cout << "between 1 and 100. You have 6 tries. If you" << endl;
	cout << "guess right you win a fraction of your bet. If" << endl;
	cout << "you guess wrong you lose your bet. Ready?" << endl;
	cout << endl;
	cout << "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" << endl;
	cout << "You have " << money << " dollars." << endl; //Informs the user of their current (starting) money
}

/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

int PlayGame(int money) //Function that plays the game
{
	int guess = 0; //What the user guesses
	int guessCounter = 0; //What guess the user is on
	int randNum; //The random number the user tries to guess
	int bet = 0; //How much the user bets
	
	//Random Number Generator (I chose not to use the one from lottery.cpp)
	srand(static_cast<unsigned>(time(NULL)));
	randNum = rand() % 100 + 1; //Equation that finds a number between 1 and 100
	//End Random Number Generator
	
	cout << "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" << endl;
	GetBet(money, bet); //Gets and confirms the bet
	cout << "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" << endl;
	//cout << randNum;
	do //This loop will continue until the guessCounter is 6, or the user's guess is equal to randnum
	{
		guessCounter++; //Increases the guess counter
		cout << "Guess " << guessCounter << ": ";
		
		guess = GetGuess() ; //Gets the user's guess
		
		if (guess > randNum) //Triggers if the guess is too high
		{
			cout << "Too high!" << endl;
		}
		else if (guess < randNum) //Triggers if the guess is too low
		{
			cout << "Too low!" << endl;
		}
		else if (guess == randNum) //Checks to see if the user won
		{
			cout << "You win after only " << guessCounter << " tries." << endl;
			cout << "You won " << (bet/guessCounter) << " dollar(s)." << endl;
		}
	}
	
	while (guess != randNum && guessCounter < 6);
	
	if (guess != randNum)//Triggers if the user lost
		{
			cout << "Sorry, you didnt win! The correct number is " << randNum << "." << endl;
			cout << "You lost " << bet << " dollar(s)." << endl;
			bet=-bet; //This program uses recognizeses that a negative bet means the user lost
		}			  //The sign of bet will help other functions determine if you won or lost
	
	money = CalcNewMoney(money, bet, guessCounter); //Calculates the users new money.
	
	cout << "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" << endl;
	
	return(money);//Returns money to main
}

/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

void GetBet(int money, int& bet) //Gets the users bet and makes sure it is acceptable
{
	
	cout << endl << "How much would you like to bet: ";
	
	cin >> bet; //gets bet

	while (bet > money || bet < 0 || !cin ) //Loops when input is negative or incorrect
	{
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		cout << "Please enter a valid bet: ";
		cin >> bet; 
	}
}

/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

int GetGuess(void) //Confirms the guess
{
	int guess; //What to return
	
	cin >> guess; //User enters the guess
	
	
	if (guess < 0 || guess > 100 || !cin ) //Loops if there is incorrect input
	{
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		guess=0; //Sets the guess to zero
	}

	return(guess); //Returns the modified input
}

/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

int CalcNewMoney(int money, int bet, int guessCounter) //Function to calculate money after the game
{
	int moneyFinal; //int to store the new money value
	
	if (bet > 0) //If the user loses the PlayGame function sets the bet negative (See PlayGame)
				// Hence if the bet is positive when it goes into this funtion, the user must have won.
	{
		moneyFinal = money+(bet/guessCounter);
		moneyFinal =-moneyFinal;
	}
	else //This triggers if the user loses.
	{
		moneyFinal = money + bet; //Since the bet is already negative, they can be simply added
	}
	
	return (moneyFinal); //Returns the money final
}

/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

bool PlayAgain(void) //Asks the user if they would like to play once more
{
	bool playAgain= false; //Used to determine if the user wants to play agian
	char playAgainResponse; //The users input
	
	do //loops for as long as the user does not enter y,Y,n, or N
	{
		cout << endl << "Would you like to play once more? (y or n): ";
		
		cin >> playAgainResponse;
		cin.sync(); //Ignores any extra characters

		//If statment to analyze the users response
		if (playAgainResponse == 'y' || playAgainResponse == 'Y')
		{
			playAgain = true;
		}
		else if (playAgainResponse == 'n' || playAgainResponse == 'N')
		{
			playAgain = false;
			cout << endl << "Thank you, come again!" << endl; //A friendly goodbye statement
		}
			
	}
			while(playAgainResponse != 'y' && playAgainResponse != 'Y'&&
				  playAgainResponse != 'n' && playAgainResponse != 'N');

	return(playAgain); //Returns wether or not the user wants to play again
}

