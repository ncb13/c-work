# README #

## Nikhil Bosshardt ##

### C++ Work ###

* This is a collection of the C++ work I did for my Computer Science minor. 
* Attached are 6 projects I found to be the most useful.
* I have included the uncompiled .cpp files for each.

###[Included Work](https://bitbucket.org/ncb13/c-work/src/fa55fd54bc954cff1a555fd5e4ca7c509e4b848b?at=master)###
1. Grade Calculator
2. IO_Printer
3. High Low Game
4. Linked List Playlist
5. Triangle Drawing
6. CYPHER