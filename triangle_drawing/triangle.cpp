/* ===========================================================================
 PROGRAM: Triangle Class .cpp

    AUTHOR: Nikhil Bosshardt
    FSU MAIL NAME: ncb13
    RECITATION SECTION NUMBER: 6
    RECITATION INSTRUCTOR NAME: David Lafontant
    COURSE: COP 3330 Fall 2014
    PROJECT NUMBER: 1 
    DUE DATE: Wednesday 9/12/14

 =========================================================================== */

#include <iostream>
#include "triangle.h"

using namespace std;

Triangle::Triangle(int s, char b, char f)
{
	// Size between 1 and 39
	if (s > 39)
		s = 39;
	else if (s < 1)
		s = 1;
	
	sideSize = s;
	borderChar = b;
	fillChar = f;

}

int Triangle::GetSize()
{
	int ret = sideSize;
	return(ret);
}

int Triangle::Perimeter()
{
	perimSize = sideSize*3;
	int ret = perimSize;
	return(ret);
}

double Triangle::Area()
{
	// Area = sqrt(3)xside^2/4
	const double sqrt3 = 1.73205080757;
	areaSize = ((sideSize*sideSize*sqrt3)/4);
	double ret = areaSize;
	return(ret);
}

void Triangle::Grow()
{
	if (sideSize <= 38)
		sideSize++;
	else if (sideSize > 38)
		sideSize = 39;
}

void Triangle::Shrink()
{
	if (sideSize <= 1)
		sideSize = 1;
	else if (sideSize > 2)
		sideSize--;
}
//
void Triangle::SetBorder(char n)
{
	if (n <'!' || n > '~')
		n = '#';
	borderChar = n;
}

void Triangle::SetFill(char f)
{
	if (f < '!' || f > '~')
		f = '*';
	fillChar = f;
}
//
void Triangle::Summary()
{
	cout << "The size of the sides is " << sideSize << endl;
	cout << "The perimeter is " << Perimeter() << endl;
	cout << "The area is " << Area() << endl;
	Draw();
}

void Triangle::Draw()
{
//vars
int size = sideSize; //varsawp
int line = 1;
char fill = fillChar; //varswap
int fillCount = -1;
char bord = borderChar; //varswap
const char sp = ' ';

//Top
for (int n=1; n<=(size-1); n++)
{
	cout << sp;
	if(n == (size-1))
	{
	cout << bord << endl;
	break;
	}
}

// Middle
for (int c=1; c<=(size-2); c++)//each line is constructed here
{
	line++;
	
	//Spaceloop
	for (int a = 1; a<=(size-line); a++)
	{
		cout << sp;
	}
	cout << bord;

	//Filling loop
	fillCount++;
	fillCount++;
	for (int a = 1; a<=fillCount;a++)
	{
		if (a %2 == 0)
			cout << fill;
		else
			cout << sp;
	}
	cout << bord << endl;
}

//bottom
for (int a = 1; a<=((size*2)-1); a++)
	{
		if (a %2 == 1)
			cout << bord;
		else
			cout << sp;
	}
	cout << endl;
}