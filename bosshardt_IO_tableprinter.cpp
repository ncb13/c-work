/* ===========================================================================
 PROGRAM: Login Interface

    AUTHOR: Nikhil Bosshardt
    FSU MAIL NAME: ncb13
    PROJECT NUMBER: 6 
    PLATFORM: Windows OS / C++ 11 / Microsoft Visual C++ Express 2012 IDE

SUMMARY:

This program first reads in a text file "FILENAME". It then analyses this file
and prints out the number of users as well as sets of usernames, passwords, 
and PIN numbers in a table format. It then displays a welcome message and 
asks the user for thier own user name, password, and PIN. If it matches one in
the data base txt file, it will accept them. If not it will give them another
chance (up to 3), and give them suggestions on on how to improve thier input.

INPUT:

One text file to be analyzed, that has the usernames, passwords, and PINs, as
well as the number of users. The user is also promtped for thier own information.

OUTPUT:

An echo print of FILENAME in a table. A welcome message. Promts for the users
information. 

ASSUMPTIONS:

The first line of FILENAME is the number of users in the file. The line after 
is a username, then a password, and then a pin number. It repeats as many times
as the first line. 

 =========================================================================== */

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

using namespace std;

/* ########################################################### */
/* #############       Typedefs and Structs      ############# */
/* ########################################################### */

struct UserRecord //Used to store database entries from FILENAME
{
   string userID;
   string password;
   int PIN;
};

struct PersonalData //Used to the data the user is prompted for
{
	string pUser; //Entered user ID
	string pPass; //Entered Password
	int pPIN; //Entered PIN
	int loginNum; //Which number the user is in the database
};

/* ########################################################### */
/* ######################  Prototypes  ####################### */
/* ########################################################### */

//Reads in the FILENAME and echoprints it in database form, returns
//the number of users.
int dataReadIn (string, UserRecord[]);

//Uses the users entered personal data to determine if the
//information matches anything
bool securtiy (int, PersonalData&, UserRecord[]);

//Displays a large welcome message
void welcometxt ();

/* ########################################################### */
/* ###################### Main Function ###################### */
/* ########################################################### */

int main ()
{
	UserRecord data[50]; //Array used to store database entries from FILENAME
	PersonalData info; //Used to store the entered information.
	
	int userAmount; //Number of total users in FILENAME
	const string FILENAME = "users.txt"; //The name of the txt file being opened
	int attempts = 0; //The login attempts counter
	string temp; //Placeholder in calculating converting sting to int of PIN
	bool loginStatus; //If the user has entered correct information
	const int MAXTRY = 3; //Maximum number of login attempts
	userAmount = dataReadIn(FILENAME, data); //Runs the database analysis
	
	welcometxt (); //Displays the welcome message

	cout << "========================== to LoginHere.net ========" << endl;
	cout << " =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" << endl;
	
	do //Loops until 3 attempts or a login.
	{
	
	//Runs if the file is empty or not readable.
	if(userAmount == 0)
	{
		cout << "====================================================" << endl;
		cout << "There was an error reading the file!" << endl;
		cout << "Please make sure that " << FILENAME << 
				" is the correct spelling!" << endl;
		cout << "====================================================" << endl;
		break;
	}

	//Promts the user for thier login information.
	cout <<         "===================================================="
		 << endl;
	cout << endl << "        Please enter your login information:        "
		 << endl;
	cout << endl << "===================================================="
		 << endl;
	cout << setw(25) << "Username: "; //Gets username
	cin >> info.pUser;
	cout << setw(25) << "Password: ";//Gets password
	cin >> info.pPass;
	cout << setw(25) << "PIN: ";//Gets PIN
	cin >> temp;
	info.pPIN = atoi(temp.c_str()); //Converts PIN to int
	cout << "====================================================" << endl
		 << endl;
	
	//Checks if the information matches.
	loginStatus = securtiy(userAmount, info, data); 
	
		if (loginStatus == true) //Runs if the user has correct information
		{
			cout << " Welcome " << data[info.loginNum].userID << "." << endl;
			cout << endl << "===================================================="
				 << endl;
		}
		else if (loginStatus == false) //Runs if the user entered bad info
		{
			attempts++; //Increases the attempt counter
			
			cout << " Wrong Information! You have " << (MAXTRY - attempts)
				 << " attempts left." << endl; //Informs the user of attempts left
			cout << endl << "===================================================="
				 << endl;
			cout << " =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" << endl;
			
			if (attempts == MAXTRY) //Runs if you have 3 attempts.
			{
				cout << "====================================================" << endl;
				cout << "Sorry, you are now locked out!" << endl;
				cout << "====================================================" << endl;
			}

		}
	}
	while((attempts > 0 && attempts < MAXTRY) && loginStatus == false);
		
		return (0);
}

/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

int dataReadIn (string FILENAME, UserRecord data[])
{
	ifstream inputFile; //The ifstream
	string c; //Variable user to grab strings from FILENAME
	int userAmount = 50; //The amount of users

	inputFile.open(FILENAME.c_str()); //Opens the file

	if (!inputFile)
	{
		userAmount = 0;
	}

	getline(inputFile, c); //Gets the first line for the user amount
	userAmount = atoi(c.c_str()); //Sets user amount to int of c
	
	//Echo prints user amount.
	cout << "====================================================" << endl;
	cout << "There are currently " << userAmount
		 << " users in the database." << endl;
	cout << "====================================================" << endl;
	
	//Gets the next string, and then stores it, loops for each user.
	for ( int count = 0; count < userAmount; count++)
	{
		getline(inputFile, c);
		data[count].userID = c;
		getline(inputFile, c);
		data[count].password = c;
		getline(inputFile, c);
		data[count].PIN = atoi(c.c_str());
	}
	
	//Database heading
	cout << setw(10) << "Username" << " | "
		 << setw(10) << "Password"  << " | "
		 << setw(5) << "PIN"  << endl;
	cout << " _______________________________________________" << endl;

	//Echoprints stored data
	for ( int count = 0; count < userAmount; count++)
	{
		cout << setw(10) << data[count].userID << " | "
			 << setw(10) << data[count].password << " | "
			 << setw(5) << data[count].PIN << endl;
	}
	
	//Message notifying that echoprint is over.
	cout << "====================================================" << endl;
	cout << "     End Echo Print of Database " << FILENAME << endl;
	cout << "====================================================" << endl;

	return (userAmount); //Returns the amount os users.

}

/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

bool securtiy (int userAmount, PersonalData &info, UserRecord data[])
{
	bool loginStatus = false; //If EVERYTHING is correct
	bool userStatus = false; //If the user name is correct
	bool passStatus = false; //If the password matches

for ( int count = 0; count < userAmount; count++) //Loops for the number of users
		{
			if ( info.pUser == data[count].userID )
			{ //Runs if the username matches anything in the database.
				userStatus = true;
				info.loginNum = count;
			}
			if (info.pPass == data[count].password && userStatus == true)
			{ //Checks if the password also matches.
				passStatus = true;
			}
			if (info.pPIN == data[count].PIN && passStatus == true)
			{ //Checks if the PIN matches as well.
				loginStatus = true;
			}
			
		}
	
	return (loginStatus);

}

/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

void welcometxt ()

{

	//A welcome message in 3d print!

cout << " __      __        __                                "<< endl;
cout << "/  \\    /  \\ ____ |  |   ____  ____   _____   ____   "<< endl;
cout << "\\   \\/\\/   // __ \\|  | _/ ___\\/  _ \\ /     \\_/ __ \\  "<< endl;
cout << " \\        /\\  ___/|  |_\\  \\__(  <_> )  Y Y  \\  ___/  "<< endl;
cout << "  \\__/\\__/  \\_____>____/\\_____>____/|__|_|__/\\____| "<< endl;
cout << endl;

}

/* ########################################################### */
/* ###################### End  Program ####################### */
/* ########################################################### */