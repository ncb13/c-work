/* ===========================================================================
PROGRAM: Class File

AUTHOR: Nikhil Bosshardt
FSU MAIL NAME: ncb13
RECITATION SECTION NUMBER: 6
RECITATION INSTRUCTOR NAME: David Lafontant
COURSE: COP 3330 Fall 2014
PROJECT NUMBER: 6
DUE DATE: Monday 11/21/14

=========================================================================== */


#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include "classes.h"

using namespace std;

/* ########################################################### */
/* #####################  Student Base  ###################### */
/* ########################################################### */

Student::Student(string l, string f)
{
	last = l;
	first = f;
	type = 0;
}

Student::Student()
{
	type = 0;
}

int Student::getType() const
{
	return type;
}

char Student::getLetter() const
{
	return letter;
}

void Student::Print(ofstream& out)
{
	string fullName = first + " " + last;
	out << left << setw(30) << fullName;
	out << left << setw(5) << finalExam;
	out << left << setw(9) << setprecision(2) << fixed << finalAvg;
	out << left << setw(5) << letter;
	out << endl;
}

/* ########################################################### */
/* ########################  Biology  ######################## */
/* ########################################################### */

Biology::Biology(int lab, int t1, int t2, int t3, int tf, string l, string f) :
				 Student(l, f)
{
	last = l;
	first = f;
	type = 1;
	finalExam = tf;

	finalAvg = (lab * .30) + (t1 * .15) + (t2 * .15) + (t3 * .15) + (tf * .25);

	if (finalAvg >= 89.5)
		letter = 'A';
	else if (finalAvg >= 79.5 && finalAvg < 89.5)
		letter = 'B';
	else if (finalAvg >= 69.5 && finalAvg < 79.5)
		letter = 'C';
	else if (finalAvg >= 59.5 && finalAvg < 69.5)
		letter = 'D';
	else if (finalAvg < 59.5)
		letter = 'F';
	else
		letter = 'E'; //Error
}

char Biology::getLetter() 
{
	return letter;
}

void Biology::Print(ofstream& out)
{
		string fullName = first + " " + last;
		out << left << setw(30) << fullName;
		out << left << setw(5) << finalExam;
		out << left << setw(9) << setprecision(2) << fixed << finalAvg;
		out << left << setw(5) << letter;
		out << endl;
}

/* ########################################################### */
/* #######################  Theater  ######################### */
/* ########################################################### */

Theater::Theater(int part, int mid, int tf, string l, string f) : Student(l, f)
{
	last = l;
	first = f;
	type = 2;
	finalExam = tf;

	finalAvg = (part * .40) + (mid * .25) + (tf * .35);

	if (finalAvg >= 89.5)
		letter = 'A';
	else if (finalAvg >= 79.5 && finalAvg < 89.5)
		letter = 'B';
	else if (finalAvg >= 69.5 && finalAvg < 79.5)
		letter = 'C';
	else if (finalAvg >= 59.5 && finalAvg < 69.5)
		letter = 'D';
	else if (finalAvg < 59.5)
		letter = 'F';
}

char Theater::getLetter() 
{
	return letter;
}

void Theater::Print(ofstream& out)
{
	string fullName = first + " " + last;
	out << left << setw(30) << fullName;
	out << left << setw(5) << finalExam;
	out << left << setw(9) << setprecision(2) << fixed << finalAvg;
	out << left << setw(5) << letter;
	out << endl;
}

/* ########################################################### */
/* #######################  Compsci  ######################### */
/* ########################################################### */

Compsci::Compsci(int p1, int p2, int p3, int p4, int p5, int p6, int t1, int t2,
				 int tf, string l, string f) : Student(l, f)
{
	last = l;
	first = f;
	type = 3;
	finalExam = tf;

	programAvg = ((p1 + p2 + p3 + p4 + p5 + p6)/6.0);
	finalAvg = (programAvg * .30) + (t1 * .20) + (t2 * .20) + (tf * .30);

	if (finalAvg >= 89.5)
		letter = 'A';
	else if (finalAvg >= 79.50 && finalAvg < 89.50)
		letter = 'B';
	else if (finalAvg >= 69.50 && finalAvg < 79.50)
		letter = 'C';
	else if (finalAvg >= 59.50 && finalAvg < 69.50)
		letter = 'D';
	else if (finalAvg < 59.50)
		letter = 'F';
}

char Compsci::getLetter() 
{
	return letter;
}

void Compsci::Print(ofstream& out)
{
	string fullName = first + " " + last;
	out << left << setw(30) << fullName;
	out << left << setw(5) << finalExam;
	out << left << setw(9) << setprecision(2) << fixed << finalAvg;
	out << left << setw(5) << letter;
	out << endl;
}
