/* ===========================================================================
PROGRAM: main.cpp File

AUTHOR: Nikhil Bosshardt
FSU MAIL NAME: ncb13
RECITATION SECTION NUMBER: 6
RECITATION INSTRUCTOR NAME: David Lafontant
COURSE: COP 3330 Fall 2014
PROJECT NUMBER: 6
DUE DATE: Monday 11/21/14

=========================================================================== */

#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "classes.h"

using namespace std;

int main()
{
	string fileIn;
	string fileOut;
	string first, last, empty, place;
	int fileSize;
	int gradeSave[10];

	cout << "Enter the file name: ";
	cin >> fileIn;
	cout << "Enter the output file name: ";
	cin >> fileOut;

	//opening the inputfile.
	ifstream text(fileIn.c_str());
	 
	//getting the number of entries
	getline(text, first);
	fileSize = atoi(first.c_str());

	//Student pointers
	Student* people = new Student[fileSize]; //The main array of students!

	/* ########################################################### */
	/* #####################  Reading Data  ###################### */
	/* ########################################################### */

	int counter = 0;
	do
	{
		getline(text, last, ',');
		getline(text, empty, ' ');
		getline(text, first);
		getline(text, empty, ' ');
		if (empty == "Computer")
		{
			getline(text, empty, ' ');
			getline(text, place, ' ');
			gradeSave[0] = atoi(place.c_str());
			getline(text, place, ' ');
			gradeSave[1] = atoi(place.c_str());
			getline(text, place, ' ');
			gradeSave[2] = atoi(place.c_str());
			getline(text, place, ' ');
			gradeSave[3] = atoi(place.c_str());
			getline(text, place, ' ');
			gradeSave[4] = atoi(place.c_str());
			getline(text, place, ' ');
			gradeSave[5] = atoi(place.c_str());
			getline(text, place, ' ');
			gradeSave[6] = atoi(place.c_str());
			getline(text, place, ' ');
			gradeSave[7] = atoi(place.c_str());
			getline(text, place);
			gradeSave[8] = atoi(place.c_str());

			people[counter] = Compsci(gradeSave[0], gradeSave[1], gradeSave[2],
				gradeSave[3], gradeSave[4], gradeSave[5],
				gradeSave[6], gradeSave[7], gradeSave[8], last, first);

		}
		else if (empty == "Biology")
		{
			getline(text, place, ' ');
			gradeSave[0] = atoi(place.c_str());
			getline(text, place, ' ');
			gradeSave[1] = atoi(place.c_str());
			getline(text, place, ' ');
			gradeSave[2] = atoi(place.c_str());
			getline(text, place, ' ');
			gradeSave[3] = atoi(place.c_str());
			getline(text, place);
			gradeSave[4] = atoi(place.c_str());
			
			people[counter] = Biology(gradeSave[0], gradeSave[1], gradeSave[2],
										gradeSave[3], gradeSave[4], last, first);
		}
		else if (empty == "Theater")
		{
			getline(text, place, ' ');
			gradeSave[0] = atoi(place.c_str());
			getline(text, place, ' ');
			gradeSave[1] = atoi(place.c_str());
			getline(text, place);
			gradeSave[2] = atoi(place.c_str());
			
			people[counter] = Theater(gradeSave[0], gradeSave[1], gradeSave[2],
										last, first);
		}
		counter++;
	} while (counter < fileSize);
	text.close();
	
	/* ########################################################### */
	/* #####################  Writing Data  ###################### */
	/* ########################################################### */

	ofstream write;
	write.open(fileOut.c_str());

	write << "Student Grade Summary" << endl;
	write << "---------------------" << endl << endl;
	write << "BIOLOGY CLASS" << endl << endl;
	write << left << setw(30) << "Student";
	write << left << setw(5) << "Final";
	write << left << setw(9) << "Final";
	write << left << setw(5) << "Letter";
	write << endl;
	write << left << setw(30) << "Name";
	write << left << setw(5) << "Exam";
	write << left << setw(9) << "Avg";
	write << left << setw(5) << "Grade";
	write << endl;
	write << "--------------------------------------------------" << endl;

	for (int i = 0; i <= fileSize; i++)
	{
		if (people[i].getType() == 1)
		{
			people[i].Print(write);
		}
	}
	
	write << endl << "THEATER CLASS" << endl << endl;
	write << left << setw(30) << "Student";
	write << left << setw(5) << "Final";
	write << left << setw(9) << "Final";
	write << left << setw(5) << "Letter";
	write << endl;
	write << left << setw(30) << "Name";
	write << left << setw(5) << "Exam";
	write << left << setw(9) << "Avg";
	write << left << setw(5) << "Grade";
	write << endl;
	write << "--------------------------------------------------" << endl;

	for (int i = 0; i <= fileSize; i++)
	{
		if (people[i].getType() == 2)
		{
			people[i].Print(write);
		}
	}
	
	write << endl  << "COMPUTER SCIENCE CLASS" << endl << endl;
	write << left << setw(30) << "Student";
	write << left << setw(5) << "Final";
	write << left << setw(9) << "Final";
	write << left << setw(5) << "Letter";
	write << endl;
	write << left << setw(30) << "Name";
	write << left << setw(5) << "Exam";
	write << left << setw(9) << "Avg";
	write << left << setw(5) << "Grade";
	write << endl;
	write << "--------------------------------------------------" << endl;
	
	for (int i = 0; i <= fileSize; i++)
	{
		if (people[i].getType() == 3)
		{
			people[i].Print(write);
		}
	}

	/* ########################################################### */
	/* ######################  Grade Dist  ####################### */
	/* ########################################################### */

	int a = 0, b = 0, c = 0, d = 0, f = 0;

	for (int i = 0; i < fileSize; i++)
	{
		if (people[i].getLetter() == 'A')
			a++;
		if (people[i].getLetter() == 'B')
			b++;
		if (people[i].getLetter() == 'C')
			c++;
		if (people[i].getLetter() == 'D')
			d++;
		if (people[i].getLetter() == 'F')
			f++;
	}
	write << endl << "OVERALL GRADE DISTRIBUTION:" << endl;
	write << left << setw(9) << "A:" << a << endl;
	write << left << setw(9) << "B:" << b << endl;
	write << left << setw(9) << "C:" << c << endl;
	write << left << setw(9) << "D:" << d << endl;
	write << left << setw(9) << "F:" << f << endl;

	//Closing
	write.close();
	cout << "Done writing output to " << fileOut << endl;
	return 0;
}