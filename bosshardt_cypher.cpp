/* ===========================================================================
 PROGRAM The DECRYPTER (Final Version)

    AUTHOR: Nikhil Bosshardt
    FSU MAIL NAME: ncb13
    PROJECT NUMBER: 5 
    PLATFORM: Windows OS / C++ 11 / Microsoft Visual C++ Express 2012 IDE

SUMMARY:

This program is capable of decoding two types of ciphers. A caesar cipher and 
a substitution cipher. The program prompts you for the type of cipher you want
to use and then prompts you to enter the name of the txt file containing the 
encoded message. The program will then read you back the original message and
the translated text, line by line, until the end of the file.

INPUT:

All input is interactive. The user will be prompted to enter a letter corresponding
to which menu option they want as well as the name of a file.

OUTPUT:

The program will output a menu, and prompts for aformentioned input. It will also out-
put echo prints and translations of encoded txt files as described in the summary.

ASSUMPTIONS:

The the text files are in the correct format. The first charactar for any caesar
cipher file will be the shift. The first 26 characters for any substitution 
cipher will be the code alphabet in its correct order. 

 =========================================================================== */

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

/* ########################################################### */
/* ######################   Typedefs   ####################### */
/* ########################################################### */

typedef char save [80];
typedef char alphaM [78];
typedef char cypher_1 [26];

/* ########################################################### */
/* ######################  Prototypes  ####################### */
/* ########################################################### */

//Prints the name and intructions for the program
void heading (); 

//Displays the menu and asks you to select one of
//three options. Returns your choice.
char menu (); 

//This function prompts the user for a file
//and then uses a subcipher to translate.
void SubCypher (); 

//This function prompts the user for a file
//and then uses a caesar cipher to translate.
void CaesarCypher ();

//Displays a simple quit message.
void Quit ();

//Prompts the user for a valid file name.
//Returns the file name.
ifstream FileGet ();

//Used in the "SubCypher" function. Input includes
//the character to be translated and the sub alphabet
//in array for.
char AnalysisSub(char, char*);

//Used in "CaesarCypher" to apply the shift to
//the current charater.
int letterAlign (char);

/* ########################################################### */
/* ###################### Main Function ###################### */
/* ########################################################### */

int main ()
{
	char choice; //Input retrieved from "menu" funtion
	
	heading (); //Prints the heading
	
	do //Loop that contains menu funtions
	{
		choice = menu ();//Displays menu options and gets the users choice
		
		switch (choice)//Runs a different function based on what the user inputs
		{
			case 'S':
			case 's': cout << "Now loading: Sub Cypher" << endl << endl;
					  SubCypher();
					  break;
			case 'C':
			case 'c': cout << "Now loading: Caesar Cypher" << endl << endl;
					  CaesarCypher();
					  break;
			case 'Q':
			case 'q': Quit();
					  break;
		}
	}
	while (choice != 'q' && choice != 'Q');
	
		return (0);

}
/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

void heading ()
{
	cout << "=================================================" << endl;
	cout << "           Welcome to The Decrypter              " << endl;
	cout << "    You have the option of performing either:    " << endl << endl;
	cout << "              a Ceaser Decryption                " << endl;
	cout << "                      or                         " << endl;
	cout << "           a Substitution Decryption             " << endl << endl;
	cout << "     You will be asked for a file to be decoded. " << endl << endl;
	cout << "The DECRYPTER will then echo print one line      " << endl;
	cout << "of your encoded text from your specified file    " << endl;
	cout << "followed by the same text decoded.               " << endl;
	cout << "=================================================" << endl << endl;
}

/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

char menu ()
{
	char menuInput; //Used to take input and is returned
	int counter = 0; //Used to trigger false statment

	 do 
		{
			if (counter > 0)
				cout << "Sorry that is not an option." << endl << endl;
			counter++;
			cout << "Please select your choice:" << endl;
			cout << "To decode a text file with the substitution cypher, press s" << endl;
			cout << "To decode a text file with the Caesar cypher, press c" << endl;
			cout << "To quit decoding, press q" << endl;
			cout << "What would you like to do? ";
			cin >> menuInput; //Input
			cin.sync(); //Ignores any extra characters
		}
	while ( menuInput != 's' && menuInput != 'c' && menuInput != 'q' &&
			menuInput != 'S' && menuInput != 'C' && menuInput != 'Q');

	return (menuInput);
}

/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

void SubCypher ()
{
	ifstream inputFile; //File being opened
	int count = 0; //Used in various for loops
	char cypher_1[26]; //The new alphabet
	char save [80]; //Saves the translation of each line
	char c; //Used to get each character
	int saveCount = -1; //Used to count place of each char on line
	
	//Heading
	
	cout <<         "=================================================" << endl;
	cout << endl << " The DECRYPTER of Substitution Scheme Encryption " << endl;
	cout << endl << "             Using Substitution set              " << endl;
	cout <<         "=================================================" << endl;
	
	inputFile = FileGet(); //Promots the user for and opens the txt file
	
	cout << endl << "=================================================" << endl;
		
	//Begin echo-print and translation 
		
	while(count < 26 && inputFile >> cypher_1[count]) //Reads in the first 26 characters from inputFile
													  //and saves them into an array
			{										  //This is the "Translated Alphabet"
				count++;
			}

	cout << "The basic alphabet is: "<< endl;
	cout << "Orginal:      ";
	for (count = 0; count < 25; count ++) //reprints the translated alphabet
		{
			cout << cypher_1[count];
		}
	cout << endl << "Translation:  ABCDEFGHIJKLMNOPQRSTUVWXYZ" << endl; //Prints normal alphabet
	cout <<         "=================================================";

	while(!inputFile.eof()) //Loops until the file ends
		{
			if (saveCount == -1) //Prints if there is a new line
			{
				cout << "      ";
			}
			saveCount++;
			inputFile.get(c); //Gets a character
			cout << c; //Prints that character
			save[saveCount] = AnalysisSub(c,cypher_1); //Saves the translation of that character
			
			if (c == '\n') //If there is a new line
			{
				cout << endl << "  --> "; // Prints an arrow to indicate a translation
				for (int count = 0; count < saveCount; count++) //Reprints the saved translations
				{
					cout << save[count];
				}
				saveCount = -1; //Resets the save counter
				cout << endl << endl;
			}
		}
	
	cout << endl << "=================================================" << endl;
	
}

/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

void CaesarCypher ()
{
	ifstream inputFile; //File being opened
	int count = 0; //Counter
	// This array has the alphabet in order. It is three times the normal size
	//To account to the shift. It is referenced when finding the shifted letter
	char alphaM[78] = {'A','B','C','D','E','F','G','H','I','J','K','L','M',
					  'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
					  'A','B','C','D','E','F','G','H','I','J','K','L','M',
					  'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
					  'A','B','C','D','E','F','G','H','I','J','K','L','M',
					  'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	char c; //Used to get letters from the txt file
	int shift; //The shift of the letters
	int saveCount =-1; //Counter for saving translations
	char save[80]; //Array used for storing translations
	
	cout <<         "=================================================" << endl;
	cout << endl << "    The DECRYPTER of Caesar Scheme Encryption    " << endl;
	cout << endl << "                Using Caesar set                 " << endl;
	cout <<         "=================================================" << endl;
	
	inputFile = FileGet(); //Function to get and open the txt file

	cout << "=================================================" << endl;
		
	inputFile.get(c); //Gets the first charater, which is the shift
	shift = c - '0'; //Converts the char shift to a integer
	shift = shift % 26; //Makes sure the shift is smaller than 26
	cout << "Your shift is " << shift << endl; //Echo prints the shift
		
	cout << "Orginal:      ";
	for (count = 0; count < 26; count ++) //Prints the normal alphabet
		{
			cout << alphaM[count];
		}
		
	cout << endl << "New Alphabet: ";
	for (count = 26; count < 52; count ++) // Prints the shifted alphabet
		{
			cout << alphaM[(count - shift)];
		}
	
	cout << endl << "================================================="
		 << endl << endl;
	
	inputFile.get(c); //Deals with empty line after shift
	
	while(!inputFile.eof()) //Start of the translation, loops until file is over
	{
		if (saveCount == -1) //Prints if there is a new line
			{
				cout << "      ";
			}
			
		saveCount++; //Increases the save counter
		inputFile.get(c); //Gets the first letter
		cout << c; //Prints that letter
			
		if (c == '.' || c == ',' || c == '\n' || c == ' ')
			{ //Runs if the char doesnt need to be shifted
				save[saveCount] = c; //Saves the result to be printed later
			}
		else
			{ //Saves the shifted translation
				save[saveCount] = alphaM[(letterAlign(c) - shift)];
			}
			
		if (c == '\n') //Runs if the line is over
			{
				cout << endl; //Prints an arrow to indiciate the start of transaltion
				cout << "  --> ";
				for (int count = 0; count < saveCount; count++) //Prints all the saved translations
					{
						cout << save[count];
					}
				saveCount = -1; //Resets the save count
				cout << endl << endl;
			}
	}
	
	cout << endl << "=================================================" << endl;
}


/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

void Quit ()
{
	cout << endl << "=================================================" << endl;
	cout <<         "        Thank you for using the DECRYPTER        " << endl;
	cout <<         "=================================================" << endl;
}

/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

char AnalysisSub(char c, char* array_1)
{
	char trans = ' '; //Returned value
	//Basic alphabet for comparison
	char alpha[27] = {'A','B','C','D','E','F','G','H','I','J','K','L','M',
					  'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

	for (int i=0; i < 25 ; i++)
	{
		if(c == array_1[i]) //Matches the two arrays
		{
			trans = alpha[i]; //Trans is the new letter
			break;
		}
	}
	//Case for punctuation and non-letters
	if (c == '\n')
		trans = '\n';
	else if (c == '.')
		trans = '.';
	else if (c == ',')
		trans = ',';
	
	return (trans);

}

/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

int letterAlign (char c)
{
	int d = 0;
	//Alphabet for comparison
	char alpha[27] = {'A','B','C','D','E','F','G','H','I','J','K','L','M',
					  'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	
	for (int i=0; i < 25 ; i++) //Runs 0-25
	{
		if (c == 'Z') //A special case for z is needed to aviod errors
		{
			d = 51;
			break;
		}
		else if(c == alpha[i]) //Checks c agaisnts every letter
		{
			d = (26 + i); //Sets d to the appropriate value
			break; //These numbers represnt positions in "alphaM"
		}
	}

		return (d);
	}

/* ########################################################### */
/* ###################### New Function ####################### */
/* ########################################################### */

ifstream FileGet ()
{
	int count = 0; //Generic counter
	string filename; //User input file name
	ifstream inputFile; //The file being opened
	
	do
	{
		if (count > 0) //Runs if there if this is run multiple times
			cout << "Sorry that wasn't a valid file name." << endl << endl;
		count++;
		cout << endl << "Enter a valid file name to decode (no spaces):";
		cin >> filename;
		inputFile.open(filename.c_str()); //Attempts to open the file
	}
		while(!inputFile);

		return (inputFile);
		
}

/* ########################################################### */
/* ###################### End  Program ####################### */
/* ########################################################### */